// -----------------------------------------------------------------------------------
// Crédits fonctions drag and drop : Le Designer du Web (https://youtu.be/-ah4LbthfYc)
// -----------------------------------------------------------------------------------

const languages = document.querySelectorAll(".languages");
const boxes = document.querySelectorAll(".boxes");
const btn = document.querySelector(".btn-check");
let currentDrag;

// "DRAG" DES <img>

// Au "drag" d'un élément...
const dragStart = ({ target }) => {
    // ... On le place dans la variable currentDrag pour pouvoir utiliser cette information par la suite
    currentDrag = target;

    // ... On fait "disparaître" l'<img> de son emplacement initial
    setTimeout(() => (target.style.opacity = 0), 0);
};

// Si l'on relache le "drag", ailleurs que dans une case, on le replace sur son emplacement initial
const dragEnd = ({ target }) => (target.style.opacity = 1);

// Avec la méthode .forEach, on parcourt toutes les <img> (stockées dans la variable languages) pour leur assigner 2 évènements via les .addEventListner
languages.forEach((lang) => {
    // ... Lorsque l'on commence à "drag", on execute la fonction dragStart (ligne 13)
    lang.addEventListener("dragstart", dragStart);

    // ... Lorsque l'on commence à "drag", on execute la fonction dragEnd (ligne 21)
    lang.addEventListener("dragend", dragEnd);
});

// "DROP" DES <img> DANS LES CASES

// Lorsque qu'un élément que l'on "drag" est au-dessus/sur une des cases
function dragOver(e) {
    e.preventDefault();
}

// Lorsque qu'un élément que l'on "drag" entre dans une case
function dragEnter(e) {
    e.preventDefault();
    this.classList.add("hovered");
}

// Lorsque qu'un élément que l'on "drag" sort d'une case
function dragLeave() {
    this.classList.remove("hovered");
    this.classList.remove("full");
}

// Lorsque que l'on "drop" dans une case un élément que l'on a drag
function dragDrop(e, box) {
    e.preventDefault(); // Pour Firefox (qui, sinon, ouvre/affiche l'image au lieu de la "drop")
    box.classList.remove("hovered");
    box.classList.add("full");
    box.append(currentDrag);
}

// Idem que pour les lignes 23-29, on assigne les fonctions, précédement déclarées, aux cases
boxes.forEach((box) => {
    box.addEventListener("dragover", dragOver);
    box.addEventListener("dragenter", dragEnter);
    box.addEventListener("dragleave", dragLeave);
    box.addEventListener("drop", (e) => dragDrop(e, box));
});

// VALIDATION
 
// Au clic du bouton...
btn.addEventListener("click", () => {
    // On parcourt toutes les cases
    boxes.forEach((box) => {
        // "Reset" de la classe de la case
        box.classList = "boxes";

        // On définit la couleur de la case (par défaut en erreur/rouge, pour les cas où les cases sont vides)
        let boxColor = "error";

        // Condition : SI il y a 2 éléments* HTML, dans une des div (avec la classe boxes donc) ...
        // (Pourquoi 2 ? De base, il y a déjà une balise code et l'on souhaite vérifier la présence)

        if (box.children.length === 2) {
            // ... On récupère (= on définit) l'attribut data-lang (html) de la case (ex: <div class="boxes" data-lang="js">)
            const boxLang = box.dataset.lang;

            // ... On récupère (= on définit) l'image qui se trouve dans la div ET qui a pour attribut data-lang le même que celui de la case
            const img = document.querySelector(
                `div[data-lang="${boxLang}"] > img`
            );

            // ... On récupère l'attribut data-lang de l'image précédemment définie
            const imgLang = img.dataset.lang;

            // ... SI les attributs data-lang de la case et de l'image sont identiques on change la couleur de la case (en valide/vert)
            if (boxLang === imgLang) boxColor = "valid";
        }

        // Ajout de la classe pour modifier la couleur de la case
        box.classList.add(boxColor);
    });
});